package cat.itb.DAOFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDeDades {

    private final String URL = "jdbc:postgresql://rogue.db.elephantsql.com:5432/"; // Ubicació de la BD.
    private final String BD = "vlejrwwq"; // Nom de la BD.
    private final String USER = "vlejrwwq";
    private final String PASSWORD = "wzI9ZeyuZdqMCnylzixZd-Cm6905Qn0h";

    //instancia de la bdd
    private static BaseDeDades database = null;
    public static BaseDeDades getInstance(){
        if(database == null)
            database = new BaseDeDades();
        return database;
    }

    private Connection connection;

    //constructor
    public BaseDeDades(){
        connection = null;
    }

    //metode per establir la conexio amb la bdd
    public Connection connect(){
        try {
            connection = DriverManager.getConnection(URL + BD, USER, PASSWORD);
            System.out.println("La base de dades s'ha iniciat correctament.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    //metode per retornar una conexio establerta
    public Connection getConnection() {
        return connection;
    }

    //metode per terminar la conexio amb la bdd
    public void close(){
        try {
            connection.close();
            System.out.println("La base de dades s'ha tancat correctament.");
        }catch(SQLException e){
            System.err.println("Error tancant la BD");
        }
        connection = null;
    }

}